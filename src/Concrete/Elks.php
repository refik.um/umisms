<?php

namespace Umisms\Sms\Concrete;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;

class Elks extends Sms
{
    private $baseUrl;

    /**
     * Class Constructor.
     * @param null $message
     */
    public function __construct($message = null)
    {
        $this->username = config('laravel-sms.elks.username');
        $this->password = config('laravel-sms.elks.password');
        $this->baseUrl = config('laravel-sms.elks.base_url', 'https://api.46elks.com');
        if ($message) {
            $this->text($message);
        }
        $headers = [
            'Authorization' => 'Basic '.base64_encode("$this->username:$this->password"),
            'Content-Type' => 'application/x-www-form-urlencoded',

        ];

        $this->client = self::getInstance();
        $this->request = new Request('POST', $this->baseUrl.'/a1/sms', $headers);
    }

    /**
     * @param null $text
     * @return bool
     */
    public function send($text = null): bool
    {
        if ($text) {
            $this->setText($text);
        }
        try {
            $request = $this->client->send($this->request, [
                'form_params' => [
                    'from' => $this->sender ?? config('laravel-sms.sender'),
                    'to' => implode(',', $this->recipients),
                    'message' => $this->text,
                ],
            ]);

            $response = json_decode($request->getBody()->getContents(), true);
            $this->response = $response;

            return $request->getStatusCode() == 200 ? true : false;
        } catch (ClientException $e) {
            logger()->error('HTTP Exception in '.__CLASS__.': '.__METHOD__.'=>'.$e->getMessage());
            $this->httpError = $e;

            return false;
        } catch (\Exception $e) {
            logger()->error('SMS Exception in '.__CLASS__.': '.__METHOD__.'=>'.$e->getMessage());
            $this->httpError = $e;

            return false;
        }
    }
}
